# Mapas ETR de INTA

Procesando mapas de ETR calculados con la metodología de Di Bella 2000, producidas por INTA.

## Interpretación del valor almacenado en las imágenes

- El pixel tiene el valor máximo de ETR para 7 ó 10 días multiplicado por 30.

## Respecto al Nombre de los archivos

Dependiendo del año, pueden ser:

- ``et31my10a18.img``
    - et  = evapotranspiración
    - 31  = día
    - my  = mes mayo (si dice en enero, fb febrero, mz marzo, ab abril, jn junio, jl julio, ag agosto, sp septiembre, oc octubre, nv noviembre y dc diciembre)
    - 10  = año 2010 (si dice 09 es 2009 y 11 es 2011)
    - a18 = satélite NOAA 18 (si dice a19 es NOAA 19)

- ``09230a19.img``
    - 09  = 2009
    - 230 = día juliano que sería el 18-agosto
    - a19 = satélite NOAA 19

- ``et09323.img``
    - et = evapotranspiración
    - 09 = año 2009
    - 323 = día juliano 19-noviembre

- ``et10082.img``
    - et  = evapotranspiración
    - 10  = año 2010
    - 082 = día juliano 23-marzo    

- ``et3decsep09recorte.img``
    - 3dec= 3ra década (21 al 30/31 depende el mes)
    - sep= septiembre
    - 09 = año 2009
    - recorte = porque es un recorte de la original
    - .img = extensión del archivo generado en ERDAS

# Períodos de interes

- 2001 .. 2007
- 2013 .. 2017